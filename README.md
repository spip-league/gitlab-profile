# La ligue des spipeureuses plutôt sympas

Le groupe `spip-league` accueille des projets de modernisation de SPIP.

Un de ses premiers objectifs est d'améliorer la robustesse du code PHP en produisant des packages installable par composer.

Rendez-vous sur le forum [discuter d'architecture](https://discuter.spip.net/c/equipes/architecture/37) pour :

- Définir une vision souhaitée à long terme du code (php, js, css) et processus (TU / CI / CD).
- En gros, comment écrire du code plus robuste, organiser les fichiers, les dépôts (par composants), pour qu’il soit logique pour des dévs, en respectant au mieux les usages des communautés des languages respectifs.
- Mettre tout ça en en pratique.
- Pour info, DX signifie « Developper Experience ».

## Plugins versus packages

La finalité des packages `spip-league` est qu'ils soient installés dans le répertoire `vendor`.

Un plugin SPIP, pour être opérationnel, doit être présent dans un des 2 dossiers suivants :

- `plugins-dist`
- `plugins`

Par conséquent, pas de plugins SPIP dans ce groupe et donc, pas de fichiers `paquet.xml`.

### Contenu des fichiers `composer.json`

- Le type de package peut être `library` (ou non renseigné), `composer-plugin`, `metapackage` et plus tard, peut-être `project`.

## Règles de nommage des projets, namespaces PHP, vendor composer

### vendor composer

Le vendor des packages PHP de ce groupe est `spip-league`

### name composer

- en lettres minuscules,
- les "mots" peuvent être séparés par des tirets simples (`-`) si besoin, pas d'underscore (~~`_`~~)
- il doit être identique au nom du projet git

### namespaces PHP

#### source code

- autoloading [PSR-4](https://www.php-fig.org/psr/psr-4/)
- Premier niveau `SpipLeague`
- Deuxième niveau parmis `Component`, `Bridge`, `Bundle`, ...
- Troisième niveau identique aau nom du projet, transformé en `CamelCase`

Exemple : projet `spip-league/logger`, namespace `SpipLeague\Component\Logger`, autoloading psr-4 `"SpipLeague\\Component\\Logger\\": "src/"`

#### tests code

- autoloading "dev" [PSR-4](https://www.php-fig.org/psr/psr-4/)
- Premiers niveaux `SpipLeague\Test`
- Troisième niveau identique au deuxième niveau du source code
- Quatrième niveau identique au troisième niveau  du source code

Exemple : projet `spip-league/logger`, namespace `SpipLeague\Test\Component\Logger`, autoloading "dev" psr-4 `"SpipLeague\\Test\\Component\\Logger\\": "tests/"`
